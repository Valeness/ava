package menu

import (
	"ava/ava"
	"github.com/dustin/go-humanize"
	"strings"
)

// ---------------
// Menu Sections
// ---------------
type Section struct {
	Name string
	Draw func(self Section)
	Selected bool
	CommandsString string
	TabName string
	Offset int
	ScrollDown func(self *Section)
	ScrollUp func(self *Section)
}

var homeHelpSection = Section{
	Name : "Home",
	Draw : func (self Section) {
		send("Commands - (Type what is inside the brackets and then hit Enter)")
		send("           (Words wrapped in curly braces ({}) are variables and should be replaced)")

		send("------------------------------------------------------------------------------------")
		send("[s]                   : Sell all Ore")
		send("[buy {ship} {amount}] : Buy a ship - Ex. [buy venture 10] will buy 10 venture ships")
		send("[equip {ship} {module_short_tag}] : Equips a module to ship - Ex. [equip venture mlu]")
		send("[upgrade {module_short_tag} {num_levels}] : Upgrades a module")
		send("------------------------------------------------------------------------------------")

		send("Press the left and right arrow keys to navigate tabs")
		send("Press the up and down arrow keys to scroll (Some ships and resources are further down the page)")
	},
	Selected : true,
	ScrollDown: func(self *Section) {

	},
	ScrollUp: func(self *Section) {

	},
}

var marketPrices = Section{
	Name : "Market Prices",
	Draw : func (self Section) {
		for _, resource := range ava.Resources[self.Offset : self.Offset + 4] {
			totalPrice := resource.BasePrice + resource.Market
			send("%s : %d", resource.Name, totalPrice)
		}
	},
	Selected : false,
	ScrollDown: func(self *Section) {
		if self.Offset < len(ava.Resources) - 4 {
			self.Offset++
		}
	},
	ScrollUp: func(self *Section) {
		if self.Offset > 0 {
			self.Offset--
		}
	},
}

func sumSlice(slice []float64) (float64) {
	var total float64 = 0
	for _, number := range slice {
		total += number
	}

	return total
}

var inventorySection = Section{
	Name : "Inventory",
	Draw : func(self Section) {
		for _, resource := range ava.Resources[self.Offset : self.Offset + 4] {
			send("%s : %s [%f/s]", resource.Name, humanize.CommafWithDigits(resource.Inventory, 3), sumSlice(resource.LastMines))
		}
	},
	Selected:false,
	CommandsString: "Sell [V]eldspar | Sell S[c]ordite",
	ScrollDown: func(self *Section) {
		if self.Offset < len(ava.Resources) - 4 {
			self.Offset++
		}
	},
	ScrollUp: func(self *Section) {
		if self.Offset > 0 {
			self.Offset--
		}
	},
}

var shipsSection = Section{
	Name : "Ships",
	Draw : func(self Section) {
		for _, ship := range ava.Ships[self.Offset : self.Offset + 2] {
			for _, module := range ship.Modules {
				ship = module.ApplyEffect(module, ship)
			}
			send("-- %s", ship.Name)
			send("---- Count : %s", humanize.Comma(ship.Count))
			send("---- Price : %s", humanize.Comma(ship.Cost))
			send("---- Ore Mined : %s", strings.Join(ship.Ore, ", "))
			send("---- MiningPower : %s", humanize.CommafWithDigits(ship.MiningPower, 5))
			send("---- Modules")

			for _, module := range ship.Modules {
				send("----- %s %d", module.Name, module.UpgradeLevel)
			}

		}
	},

	Selected: false,
	CommandsString: "Buy V[e]nture : Buy H[u]lk : Buy E[x]humer : Buy [P]rospect",
	ScrollDown: func(self *Section) {
		if self.Offset < len(ava.Ships) - 2 {
			self.Offset++
		}
	},
	ScrollUp: func(self *Section) {
		if self.Offset > 0 {
			self.Offset--
		}
	},
}

var shopSection = Section{
	Name : "Shop",
	Draw : func(self Section) {
		send("Welcome to the Shop!")
		send("")
		for _, module := range ava.Modules {
			send("[%s] %s", module.ShortTag, module.Name)
			send("--%s", module.Description)
			send("--Upgrade Cost : %s", humanize.Commaf(module.UpgradeCost))
		}
	},
	Selected: false,
	ScrollDown: func(self *Section) {
		if self.Offset < len(ava.Upgrades) - 2 {
			self.Offset++
		}
	},
	ScrollUp: func(self *Section) {
		if self.Offset > 0 {
			self.Offset--
		}
	},
}

var Sections = []Section{
	homeHelpSection,
	marketPrices,
	inventorySection,
	shipsSection,
	shopSection,
}

// -------------------
// End Menu Sections
// -------------------
