package menu

import (
	"ava/ava"
	"fmt"
	"github.com/dustin/go-humanize"
	"github.com/nsf/termbox-go"
)

var ResponseMessage = ""
var Pass string
var Input []byte
var drawY = 0
func send(format string, arguments ...interface{}) {
	if len(arguments) > 0 {
		term_print(0, drawY, termbox.ColorDefault, termbox.ColorDefault, format, arguments...)
	} else {
		term_print(0, drawY, termbox.ColorDefault, termbox.ColorDefault, format)
	}

	drawY++
}

func drawMenu() {
	menuString := "|"
	for _, section := range Sections {

		surround := ""

		if section.Selected {
			surround = "**"
		}

		menuString += " " + surround + section.Name + surround + " |"
	}

	send(menuString)
}

func print_tb(x, y int, fg, bg termbox.Attribute, msg string) {
	for _, c := range msg {
		termbox.SetCell(x, y, c, fg, bg)
		x++
	}
}

func term_print(x, y int, fg, bg termbox.Attribute, format string, args ...interface{}) {
	s := fmt.Sprintf(format, args...)
	print_tb(x, y, fg, bg, s)
}

func Draw() {

	_ = termbox.Clear(termbox.ColorDefault, termbox.ColorDefault)
	send("----------")
	send("| ISK : %s | Message: %s", humanize.CommafWithDigits(ava.ISK, 2), ResponseMessage)
	send("| Password : %s ", Pass)
	send("----------")

	drawMenu()

	for _, section := range Sections {
		if section.Selected {
			send("------------------------------------")

			if section.CommandsString != "" {
				send(section.CommandsString)
			}

			section.Draw(section)
			send("------------------------------------")

		}
	}

	send("Command : %s", string(Input))

	_ = termbox.Flush()
	drawY = 0
}

func GetSelectedSection() (int, Section) {
	returnIdx := 0
	returnSection := Sections[returnIdx]
	for k, section := range Sections {
		if section.Selected {
			returnSection = section
			returnIdx = k
		}
	}

	return returnIdx, returnSection
}

func SelectSection(sectionName string) {
	for idx, section := range Sections {
		if section.Name == sectionName {
			Sections[idx].Selected = true
		} else {
			Sections[idx].Selected = false
		}
	}
}