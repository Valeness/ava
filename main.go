package main

import (
	"ava/ava"
	"ava/menu"
	"encoding/json"
	"fmt"
	"github.com/dustin/go-humanize"
	"github.com/nsf/termbox-go"
	"io/ioutil"
	"log"
	"math/rand"
	"os"
	"strconv"
	"strings"
	"time"
)

// Global Variable Declaration
var quit = false

type Event struct {
	Locked bool
	Dispatch func(*Event) bool
	callable func()
}

func NewEvent(e Event) Event {

	e.Locked = false
	e.Dispatch = func(self *Event) bool {
		if !self.Locked {
			events = append(events, self)
			self.Locked = true
		}
		return self.Locked
	}

	return e
}

var Handler = NewEvent(Event{})

var Mine = NewEvent(Event{
	callable: func() {
		for k, r := range ava.Resources {
			if r.Name == "Veldspar" {
				ava.Resources[k].Inventory += 0.0025
			}
		}
	},
})

var events = make([]*Event, 0)

func pollEvents() {
loop:
	for {
		switch ev := termbox.PollEvent(); ev.Type {
		case termbox.EventKey:

			switch ev.Key {

			case termbox.KeyCtrlBackslash:
				Handler.Dispatch(&Mine)
				break

			case termbox.KeyCtrlC:
				quit = true
				break loop

			case termbox.KeyArrowDown:
				idx, section := menu.GetSelectedSection()
				section.ScrollDown(&menu.Sections[idx])
				break

			case termbox.KeyArrowUp:
				idx, section := menu.GetSelectedSection()
				if section.Offset == 0 && len(commandHistory) > 0 {
					lastIdx := len(commandHistory) - 1
					menu.Input = []byte(commandHistory[lastIdx])
					commandHistory = commandHistory[:lastIdx]
				} else {
					section.ScrollUp(&menu.Sections[idx])
				}
				break

			case termbox.KeyArrowRight:
				idx, _ := menu.GetSelectedSection()
				if idx + 1 <= len(menu.Sections) - 1 {
					menu.SelectSection(menu.Sections[idx + 1].Name)
				}
				break

			case termbox.KeyArrowLeft:
				idx, _ := menu.GetSelectedSection()
				if idx - 1 >= 0 {
					menu.SelectSection(menu.Sections[idx - 1].Name)
				}
				break

			case termbox.KeyEnter:
				doCommand(string(menu.Input))
				menu.Input = make([]byte, 0)
				break

			case termbox.KeySpace:
				menu.Input = append(menu.Input, byte(' '))
				break

			case termbox.KeyBackspace:
			case termbox.KeyBackspace2:
				if len(menu.Input) > 0 {
					menu.Input = menu.Input[:len(menu.Input) - 1]
				}
				break

			default:
				menu.Input = append(menu.Input, []byte(string(ev.Ch))[:]...)
				break

			}
		case termbox.EventError:
			panic(ev.Err)
		}
	}
}


func tickUpdate() {

	for _, ship := range ava.Ships {

		for _, module := range ship.Modules {
			ship = module.ApplyEffect(module, ship)
		}

		ore_idx := rand.Intn(len(ship.Ore))
		ore := ship.Ore[ore_idx]

		for idx, resource := range ava.Resources {
			if resource.Name == ore {
				mined := (float64(ship.Count) * ship.MiningPower) * (float64(tickRate) / 1000.0)
				ava.Resources[idx].Inventory += mined
				ava.Resources[idx].TickMined += mined
			}
		}

	}

	for idx, resource := range ava.Resources {
		ava.Resources[idx].LastMines = append(resource.LastMines, resource.TickMined)
		ava.Resources[idx].TickMined = 0

		if len(resource.LastMines) > 1000 / tickRate {
			ava.Resources[idx].LastMines = ava.Resources[idx].LastMines[1:]
		}

	}

	updateMarketPrices()

	if tick % 500000 == 0 {
		menu.ResponseMessage = "Here yo"
		saveGame()
		tick = 0
	}
	tick++

}

func updateMarketPrices() {
	for idx, resource := range ava.Resources {
		decision := rand.Intn(100)

		if decision <= 5 && resource.BasePrice + resource.Market != resource.BasePrice * 3 {
			ava.Resources[idx].Market++
		} else if decision >= 95 && resource.BasePrice - resource.Market != 0 {
			ava.Resources[idx].Market--
		}
	}
}

var commandHistory = make([]string, 0)

func marshalStruct(s interface{}) string {
	m, err := json.Marshal(s)

	if err != nil {
		log.Fatalln(err)
	}

	return string(m)
}

func getSaveData() string {

	ships := marshalStruct(ava.Ships)
	modules := marshalStruct(ava.Modules)
	upgrades := marshalStruct(ava.Upgrades)
	resources := marshalStruct(ava.Resources)

	payload := fmt.Sprintf("{\"ships\" : %s, \"modules\" : %s, \"upgrades\" : %s, \"resources\" : %s, \"isk\" : %f}", ships, modules, upgrades, resources, ava.ISK)

	return payload
}

const letterBytes = "abcdefghijklmnopqrstuvwxyz"

func RandStringBytes(n int) string {
	rand.Seed(time.Now().UTC().UnixNano())
	b := make([]byte, n)
	for i := range b {
		b[i] = letterBytes[rand.Intn(len(letterBytes))]
	}
	return string(b)
}

type LoadSave struct {
	Ships []ava.Ship `json:"ships"`
	Modules []ava.Module `json:"modules"`
	Upgrades []ava.Upgrade `json:"upgrades"`
	Resources []ava.Resource `json:"resources"`
	ISK float64 `json:"isk"`
}

func makeSaveDir() {
	if _, err := os.Stat("./saves"); os.IsNotExist(err) {
		err = os.Mkdir("./saves", 0755)
		if err != nil {
			log.Fatalln(err)
		}
	}
}

func saveGame() {
	if menu.Pass == "" {
		menu.Pass = RandStringBytes(5)
	}
	filename := "./saves/" + menu.Pass + ".json"

	makeSaveDir()
	saveData := getSaveData()

	if _, err := os.Stat(filename); os.IsNotExist(err) {
		f, err := os.Create(filename)

		defer f.Close()

		if err != nil {
			log.Fatalln(err)
		}

		_, err = f.WriteString(saveData)

		if err != nil {
			log.Fatalln(err)
		}

		err = f.Sync()

		if err != nil {
			log.Fatalln(err)
		}
	} else {
		err := ioutil.WriteFile(filename, []byte(saveData), 0755)
		if err != nil {
			log.Fatalln(err)
		}
	}

	menu.ResponseMessage = "Saved!!"
}

func doCommand(cmd string) {
	if len(cmd) == 0 {
		menu.ResponseMessage = "Please Enter a Command"
		return
	}
	menu.ResponseMessage = ""

	cmd = strings.ToLower(cmd)
	cmdArgs := strings.Fields(cmd)
	switch(cmdArgs[0]) {

		case "buy":
			cmdArgs := strings.Fields(cmd)
			if len(cmdArgs) > 1 {
				ship := cmdArgs[1]

				if len(cmdArgs) > 2 {
					amount, err := strconv.Atoi(cmdArgs[2])
					if err != nil {
						log.Fatalln(err)
					}
					buyShip(ship, int64(amount))
				} else {
					buyShip(ship, 1)
				}


			}
		break

		case "equip":

			cmdArgs := strings.Fields(cmd)
			if len(cmdArgs) > 2 {
				shipName := cmdArgs[1]
				moduleTag := cmdArgs[2]

				equipModule(shipName, moduleTag)
				menu.ResponseMessage = "Equipped module ["+moduleTag+"] to [" + shipName + "]"
			}

		break

		case "load":
			cmdArgs := strings.Fields(cmd)
			if len(cmdArgs) > 1 {
				menu.Pass = cmdArgs[1]

				makeSaveDir()
				filename := "./saves/" + menu.Pass + ".json"

				if _, err := os.Stat(filename); !os.IsNotExist(err) {
					data, err := ioutil.ReadFile(filename)

					if err != nil {
						log.Println("Readfile error")
						log.Fatalln(err)
					}

					ls := LoadSave{}

					err = json.Unmarshal(data, &ls)
					if err != nil {
						log.Println("UnMarshal Error")
						log.Fatalln(err)
					}

					ava.Ships = ls.Ships
					ava.Modules = ls.Modules
					ava.Upgrades = ls.Upgrades
					ava.Resources = ls.Resources
					ava.ISK = ls.ISK

					menu.ResponseMessage = "Loaded save [" + menu.Pass + "]"

				} else {
					menu.ResponseMessage = "Error Loading"
				}

			} else {
				menu.ResponseMessage = "Need more commands"
			}
		break

		case "save":

			saveGame()

		break

		case "upgrade":
			cmdArgs := strings.Fields(cmd)
			if len(cmdArgs) > 1 {
				upgrade := cmdArgs[1]

				if len(cmdArgs) > 2 {
					amount, err := strconv.Atoi(cmdArgs[2])
					if err != nil {
						log.Fatalln(err)
					}
					buyUpgrade(upgrade, int64(amount))
				} else {
					buyUpgrade(upgrade, 1)
				}


			}
		break

		case "s":
			sellOre("all")
		break

		case "?":
			ava.ISK += 100000000
		break
	}
}

func sellOre(ore string) {

	for idx, resource := range ava.Resources {
		if ore == "all" || resource.Name == ore {
			ava.ISK += resource.Inventory * float64(resource.BasePrice + resource.Market)
			ava.Resources[idx].Inventory = 0
		}
	}
}

func doUpgrade(module *ava.Module) {
	if  ava.ISK >= float64(module.UpgradeCost) {
		module.UpgradeLevel++
		ava.ISK -= float64(module.UpgradeCost)
		module.UpgradeCost = module.UpgradeCost * 10

		menu.ResponseMessage = "Successfully upgraded " + module.ShortTag + " for " + humanize.Commaf(module.UpgradeCost) + " ISK"
	} else {
		menu.ResponseMessage = "You cannot afford this upgrade!"
	}
}

func getShipByName(shipName string) (int, ava.Ship) {
	for idx, ship := range ava.Ships {
		if ship.Name == shipName {
			return idx, ava.Ships[idx]
		}
	}

	return 0, ava.Ship{}
}

func getModuleByTag(moduleTag string) (int, ava.Module) {
	for idx, module := range ava.Modules {
		if module.ShortTag == moduleTag {
			return idx, ava.Modules[idx]
		}
	}

	return 0, ava.Module{}
}

// I'm thinking that the player should pay an "Equip Fee" if they already have ships purchased
// This means they would have to pay the difference in cost from when they bought the ship to what the ship would cost with the upgrade
// I want to prevent them buying 10m ventures and then equipping a upgrade that would make each venture cost 100k or something
func equipModule(moduleTag string, shipName string) {

	shipIdx, ship := getShipByName(strings.ToLower(shipName))
	idx, _ := getModuleByTag(moduleTag)

	ava.Ships[shipIdx].Modules = append(ship.Modules, &ava.Modules[idx])
}

func buyUpgrade(upgradeName string, amount int64) {

	idx, _ := getModuleByTag(upgradeName)

	for i := int64(0); i < amount; i++ {
		doUpgrade(&ava.Modules[idx])
	}
}

func buyShip(shipName string, amount int64) {

	for idx, ship := range ava.Ships {

		if strings.ToLower(ship.Name) != strings.ToLower(shipName) {
			continue
		}

		for _, module := range ship.Modules {
			ship = module.ApplyEffect(module, ship)
		}

		totalCost := float64(ship.Cost * amount)
		if  ava.ISK >= totalCost {
			ava.Ships[idx].Count += amount
			ava.ISK -= totalCost

			menu.ResponseMessage = "Successfully bought " + humanize.Comma(amount) + " " + shipName + "(s) for " + humanize.Commaf(totalCost) + " ISK"
		} else {
			menu.ResponseMessage = "You cannot afford this ship!"
		}
	}

}

var tickRate = 50
var tick = 0

func main() {

	err := termbox.Init()
	if err != nil {
		panic(err)
	}
	defer termbox.Close()

	termbox.SetInputMode(termbox.InputEsc | termbox.InputMouse)

	// Always Polls Events
	go pollEvents()

	// Render Loop
	for {
		if quit {
			break
		}

		for _, ev := range events {
			ev.callable()
			ev.Locked = false
		}

		events = make([]*Event, 0)

		tickUpdate()

		menu.Draw()
		time.Sleep(time.Duration(tickRate) * time.Millisecond)
	}
}
