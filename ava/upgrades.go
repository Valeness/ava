package ava

import "encoding/json"

type Upgrade struct {
	Name string `json:"name"`
	ShortTag string `json:"short_tag"`
	Description string `json:"description"`
	Cost int64 `json:"cost"`
	UpgradeLevel float64 `json:"upgrade_level"`
	ShipName string `json:"ship_name"`
	Effect func(self *Upgrade, ship *Ship) `json:",omitempty"`
}

func (u *Upgrade) UnmarshalJSON(data []byte) error {
	aux := &struct {
		Name string `json:"name"`
		ShortTag string `json:"short_tag"`
		Description string `json:"description"`
		Cost int64 `json:"cost"`
		UpgradeLevel float64 `json:"upgrade_level"`
		ShipName string `json:"ship_name"`
	}{
		u.Name,
		u.ShortTag,
		u.Description,
		u.Cost,
		u.UpgradeLevel,
		u.ShipName,
	}

	if err := json.Unmarshal(data, &aux); err != nil {
		return err
	}

	return nil
}

func (u *Upgrade) MarshalJSON() ([]byte, error) {
	return json.Marshal(&struct {
		Name string `json:"name"`
		ShortTag string `json:"short_tag"`
		Description string `json:"description"`
		Cost int64 `json:"cost"`
		UpgradeLevel float64 `json:"upgrade_level"`
		ShipName string `json:"ship_name"`
	}{
		u.Name,
		u.ShortTag,
		u.Description,
		u.Cost,
		u.UpgradeLevel,
		u.ShipName,
	})
}

type Module struct {
	Name string `json:"name"`
	ShortTag string `json:"short_tag"`
	Description string `json:"description"`
	UpgradeCost float64 `json:"upgrade_cost"`
	CostMultiplier float64 `json:"cost_multiplier"` // The additional price added to a ship when equipped
	UpgradeLevel int `json:"upgrade_level"`
	ApplyEffect func(self *Module, ship Ship) Ship
}

func (m *Module) MarshalJSON() ([]byte, error) {
	return json.Marshal(&struct {
		Name string `json:"name"`
		ShortTag string `json:"short_tag"`
		Description string `json:"description"`
		UpgradeCost float64 `json:"upgrade_cost"`
		CostMultiplier float64 `json:"cost_multiplier"`
		UpgradeLevel int `json:"upgrade_level"`
	}{
		m.Name,
		m.ShortTag,
		m.Description,
		m.UpgradeCost,
		m.CostMultiplier,
		m.UpgradeLevel,
	})
}

func (m *Module) UnmarshalJSON(data []byte) error {
	aux := &struct {
		Name string `json:"name"`
		ShortTag string `json:"short_tag"`
		Description string `json:"description"`
		UpgradeCost float64 `json:"upgrade_cost"`
		CostMultiplier float64 `json:"cost_multiplier"`
		UpgradeLevel int `json:"upgrade_level"`
	}{
		m.Name,
		m.ShortTag,
		m.Description,
		m.UpgradeCost,
		m.CostMultiplier,
		m.UpgradeLevel,
	}

	if err := json.Unmarshal(data, &aux); err != nil {
		return err
	}

	return nil
}

var miningLaserUpgrade = Module{
	Name : "Mining Laser Upgrade",
	ShortTag : "mlu",
	Description : "Increases yield of mining operations",
	CostMultiplier : 5,
	UpgradeCost : 100,
	UpgradeLevel: 1,
	ApplyEffect: func(self *Module, ship Ship) Ship {
		ship.Cost = int64(float64(ship.Cost) * (self.CostMultiplier * float64(self.UpgradeLevel)))
		ship.MiningPower += .015 * float64(self.UpgradeLevel)
		return ship
	},
}

var Modules = []Module{
	miningLaserUpgrade,
}

var ventureMiningTurret = Upgrade{
	Name : "Venture Mining Turret Upgrade",
	ShortTag : "vmt",
	Description : "Increases mining power of all Venture Ships by .001",
	Cost : 100,
	Effect : func(self *Upgrade, ship *Ship) {
		self.UpgradeLevel += 1
		ship.MiningPower += 0.001
		self.Cost = self.Cost * 10
	},
	ShipName : "Venture",
}

var prospectMiningTurret = Upgrade{
	Name : "Prospect Mining Turret Upgrade",
	ShortTag : "pmt",
	Description : "Increases mining power of all Prospect Ships by .001",
	Cost : 5000,
	Effect : func(self *Upgrade, ship *Ship) {
		self.UpgradeLevel += 1
		ship.MiningPower += 0.001
		self.Cost = self.Cost * 10
	},
	ShipName : "Prospect",
}

var Upgrades = []Upgrade{
	ventureMiningTurret,
	prospectMiningTurret,
}
