package ava

type Resource struct {
	Name string
	BasePrice int64
	Market int64
	Inventory float64
	Hardness float64
	LastMines []float64
	TickMined float64
}

var ISK float64 = 10

var Veldspar = Resource{
	Name : "Veldspar",
	BasePrice:5,
	Market:0,
	Inventory:0,
}

var Scordite = Resource{
	Name : "Scordite",
	BasePrice:40,
	Market:0,
	Inventory:0,
	Hardness : 0.15,
}

var Kernite = Resource{
	Name : "Kernite",
	BasePrice:65,
	Market:-5,
	Inventory:0,
	Hardness : 0.3,

}

var Pyroxeres = Resource{
	Name : "Pyroxeres",
	BasePrice:90,
	Inventory:0,
	Hardness : 0.45,
}

var Omber = Resource{
	Name : "Omber",
	BasePrice:20,
	Inventory:0,
	Hardness : 0.6,
}

var Resources = []Resource{
	Veldspar,
	Scordite,
	Kernite,
	Pyroxeres,
	Omber,
}
