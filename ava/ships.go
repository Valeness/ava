package ava

type Ship struct {
	Cost int64
	MiningPower float64
	Count int64
	Name string
	Ore []string
	Modules []*Module
}

// Frigate
var Venture = Ship{
	Cost : 10,
	Name : "Venture",
	Count : 1,
	MiningPower : 0.01,
	Ore : []string{
		"Veldspar",
	},
}

// Expedition Frigate
var Prospect = Ship{
	Cost : 5000,
	Name : "Prospect",
	Count : 0,
	MiningPower : .1,
	Ore : []string{
		"Veldspar",
	},
}

var Endurance = Ship{
	Cost : 100000,
	Name : "Endurance",
	Count : 0,
	MiningPower : 0.05,
	Ore : []string{
		"Kernite",
	},
}

// Mining Barge
var Procurer = Ship{
	Cost : 150000,
	Name : "Procurer",
	Count : 0,
	MiningPower : 0.15,
	Ore : []string{
		"Veldspar",
		"Scordite",
		"Pyroxeres",
	},
}

var Covetor = Ship{
	Cost : 175000,
	Name : "Covetor",
	Count : 0,
	MiningPower : 0.18,
	Ore : []string{
		"Veldspar",
		"Scordite",
		"Omber",
	},
}

// Exhumer
var Skiff = Ship{
	Cost : 95000,
	Name : "Skiff",
	Count : 0,
	MiningPower : .08,
	Ore : []string{
		"Kernite",
		"Omber",
	},
}

var Hulk = Ship{
	Cost : 50000,
	Name : "Hulk",
	Count : 0,
	MiningPower : .15,
	Ore : []string{
		"Veldspar",
		"Scordite",
	},
}

var Exhumer = Ship{
	Cost : 150000,
	Name : "Exhumer",
	Count : 0,
	MiningPower : .25,
	Ore : []string{
		"Kernite",
		"Pyroxeres",
		"Omber",
	},
}

var Rorqual = Ship{
	Cost : 150000000,
	Name : "Rorqual",
	Count : 0,
	MiningPower : 1.25,
	Ore : []string{
		"Kernite",
		"Pyroxeres",
		"Omber",
	},
}

var Ships = []Ship{
	Venture,
	Prospect,
	Endurance,
	Procurer,
	Covetor,
	Skiff,
	Hulk,
	Exhumer,
	Rorqual,
}