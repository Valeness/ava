# AVA
Like EVE Online but without all the pesky graphics

## Build
```
# Just build all the binaries
scripts/build.sh

# Build binaries and upload them to gitlab for release tagging
cp .env.example .env # Fill out .env with private token and project id

source .env && scripts/build.sh upload
```

## Run
```
cd ~/go/src/
git clone git@gitlab.com:Valeness/ava.git
cd ava/
go build -o ~/go/bin/ava main.go
~/go/bin/ava
```

## How to Play

##### Commands
Commands are also in the "Help Tab" of the game

```
-- Keyboard Commands --
Right/Left Arrows : Move Tabs (Ex. Move from ships to shop tab)
Up/Down Arrows    : Scroll up and down inside current tab (Ex. To see more ships or resources)

-- Text Commands --
send("[s]                   : Sell all Ore")
send("[buy {ship} {amount}] : Buy a ship - Ex. [buy venture 10] will buy 10 venture ships")
send("[equip {ship} {module_short_tag}] : Equips a module to ship - Ex. [equip venture mlu]")
send("[upgrade {module_short_tag} {num_levels}] : Upgrades a module")
```

--DEPRECATED COMMAND STRUCTURE (some still work)--

Most commands are encapsulated in brackets. For example if you see the text `Sell S[c]ordite`, then if you
enter the command `c` into the console then you will sell your Scordite Ore

##### Gameplay
The point of the game is to accumulate as much `ISK` as possible. Do this by buying different ships to mine
different kinds of ore in order to sell that ore for `ISK`. In turn, you use `ISK` to buy and upgrade 
more ships that can then in turn mine more ore, to sell for more `ISK` and so on...
